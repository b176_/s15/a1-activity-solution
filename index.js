// first part
let firstNumber = parseInt(prompt('Enter first number:'))
let secondNumber = parseInt(prompt('Enter second number:'))
let total = firstNumber + secondNumber
let finalNumber
let finalText

if(total < 10){
    finalNumber = firstNumber + secondNumber
    finalText = "total"
}else if(total < 20){
    finalNumber = firstNumber - secondNumber
    finalText = "difference"
}else if(total < 30){
    finalNumber = firstNumber * secondNumber
    finalText = "product"
}else{
    finalNumber = firstNumber / secondNumber
    finalText = "quotient"
}

(finalNumber < 10) ? console.warn(finalNumber) : alert(`The ${finalText} of the two numbers is: ${finalNumber}`)
// end of first part

// second part
let userName = prompt('Enter your name:')
let userAge = prompt('Enter your age:')

if(!userName || !userAge)
{
    alert('Are you a time traveler?')
}else if(userName && userAge)
{
    alert(`Your name is ${userName}, and you are ${userAge} years old!`)
}

function isLegalAge()
{
    if(userAge < 18)
    {
        alert('You are not allowed here.')
    }else{
        alert('You are of legal age.')
    }
}

switch(parseInt(userAge)){
    case 18:
        alert('You are now allowed to party')
        break
    case 21:
        alert('You are now part of the adult society')
        break
    case 65:
        alert('We thank you for your contribution to society')
        break
    default:
        alert("Are you sure you're not an alien?")
        break
}

try{

}
catch{

}
finally{
    console.warn('Error found!')
    isLegalAge()
}
// end of second part
